import { NgModule } from '@angular/core';
import { AppLayoutComponent } from './app-layout.component';

@NgModule({
  declarations: [
    AppLayoutComponent,
  ],
  exports: [
    AppLayoutComponent,
  ],
})
export class AppLayoutModule {
}
