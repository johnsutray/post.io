import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'post-io';

  constructor(
    public http: HttpClient,
  ) {
  }

  ngOnInit() {
    this.http.get(`${environment.apiEndpoint}/users`).subscribe(console.log);
  }
}
